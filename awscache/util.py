import boto3


class AwsConnectionDetails(object):
    class __AwsConnectionDetails:
        def __init__(self):
            self.identity = AwsIdentity()
            self.region = boto3.session.Session().region_name

    connection = None

    def __init__(self):
        if not AwsConnectionDetails.connection:
            AwsConnectionDetails.connection = AwsConnectionDetails.__AwsConnectionDetails()

    def get_identity(self):
        return self.connection.identity

    def get_account(self):
        return self.get_identity()['account']

    def get_region(self):
        return self.connection.region

    def get_username(self):
        return AwsArn.parse_arn(self.get_identity()['arn'])['iam_user']

    def get_userpath(self):
        return AwsArn.parse_arn(self.get_identity()['arn'])['iam_path']

    @staticmethod
    def current_identity():
        return AwsConnectionDetails().get_identity()

    @staticmethod
    def current_region():
        return AwsConnectionDetails().get_region()


class AwsArn:
    @staticmethod
    def parse_arn(arn):
        # arn:aws:service:region:account:resource
        elements = arn.split(':')
        result = {
            'arn': elements[0],
            'aws': elements[1],
            'service': elements[2],
            'region': elements[3],
            'account': elements[4],
            'resource': elements[5]
        }

        if result['service'] == 'iam':
            iam_elements = result['resource'].split('/')
            result['iam_path'] = iam_elements[0:-1]
            result['iam_user'] = iam_elements[-1]

        return result


class AWSCfnDetails:
    def __init__(self, stack_id=None, stack_name=None, logical_id=None):
        self.stack_id = stack_id
        self.stack_name = stack_name
        self.logical_id = logical_id


class AwsIdentity(dict):
    def __init__(self, identity=boto3.client('sts').get_caller_identity()):
        dict.__init__(self, account=identity['Account'],
                      arn=identity['Arn'],
                      user_id=identity['UserId'])


class AwsTagsUtil:
    TAG_NAME = 'Name'
    TAG_DESCRIPTION = 'Description'
    TAG_CFN_STACK_ID = 'aws:cloudformation:stack-id'
    TAG_CFN_STACK_NAME = 'aws:cloudformation:stack-name'
    TAG_CFN_STACK_LOGICAL_ID = 'aws:cloudformation:logical-id'

    @staticmethod
    def extract_name_from_tags(tags_list):
        return next((item['Value'] for item in tags_list if item["Key"] == AwsTagsUtil.TAG_NAME), None)

    @staticmethod
    def extract_item_from_tags(tag_name, tags_list):
        return next((item['Value'] for item in tags_list if item["Key"] == tag_name), None)

    @staticmethod
    def extract_cloudformation_from_tags(tags_list):
        cfn_id = next((item['Value'] for item in tags_list if item["Key"] == AwsTagsUtil.TAG_CFN_STACK_ID), None)
        if cfn_id:
            return AWSCfnDetails(cfn_id,
                                 next(
                                     (item['Value'] for item in tags_list if
                                      item["Key"] == AwsTagsUtil.TAG_CFN_STACK_NAME),
                                     None),
                                 next(
                                     (item['Value'] for item in tags_list if
                                      item["Key"] == AwsTagsUtil.TAG_CFN_STACK_LOGICAL_ID), None))

        return AWSCfnDetails()
