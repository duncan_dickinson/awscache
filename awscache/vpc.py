import boto3

from .base import BaseResource
from .model.models import VpcModel
from .util import AwsTagsUtil


class Vpcs(BaseResource):
    __model__ = VpcModel
    __resource_name__ = "VPCs"

    @staticmethod
    def list():
        client = boto3.client('ec2')
        vpc_list = client.describe_vpcs()['Vpcs']
        result = []
        for aws_vpc in vpc_list:
            vpc = VpcModel(id=aws_vpc['VpcId'],
                           name=AwsTagsUtil.extract_name_from_tags(aws_vpc['Tags']),
                           cidr_block=aws_vpc['CidrBlock'],
                           enable_dns_support=client.describe_vpc_attribute(Attribute='enableDnsSupport',
                                                                            VpcId=aws_vpc['VpcId']),
                           enable_dns_hostnames=client.describe_vpc_attribute(Attribute='enableDnsHostnames',
                                                                              VpcId=aws_vpc['VpcId']),
                           cfn_details=AwsTagsUtil.extract_cloudformation_from_tags(aws_vpc['Tags']))
            result.append(vpc)
        return result
