import boto3

from .base import BaseResource
from .model.models import IamGroupMembershipModel, IamGroupModel


class IamGroups(BaseResource):
    __model__ = IamGroupModel
    __resource_name__ = "IAM Groups"

    @staticmethod
    def list():
        client = boto3.client('iam')
        iam = boto3.resource('iam')

        result = []
        group_list = []

        listing_response = client.list_groups()
        while True:
            for group in listing_response['Groups']:
                group_list.append(group['GroupName'])

            if listing_response['IsTruncated']:
                listing_response = client.list_groups(Marker=listing_response['Marker'])
            else:
                break

        for group_name in group_list:
            group = iam.Group(group_name)
            result.append(IamGroupModel(
                id=group.group_id,
                name=group.group_name,
                arn=group.arn,
                create_date=group.create_date,
                path=group.path
            ))
            for user in group.users.all():
                result.append(IamGroupMembershipModel(
                    user_id=user.user_id,
                    group_id=group.group_id
                ))

        return result
