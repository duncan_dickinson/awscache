#!/usr/bin/env python3

import logging

from .awscache import AwsCache


def main():
    import argparse
    from os.path import expanduser

    parser = argparse.ArgumentParser(prog='AWS Resource Cache',
                                     description='Queries AWS resources and caches results in a database',
                                     add_help=True,
                                     formatter_class=argparse.ArgumentDefaultsHelpFormatter)

    parser.add_argument('-f', '--database-file',
                        help='The database file',
                        default='{}/cache.db'.format(expanduser('~/.awscache')),
                        dest='db_path')

    parser.add_argument('--log',
                        help='The logging level',
                        default='info',
                        dest='loglevel')

    parser.add_argument('--refresh',
                        help='Refresh the locally cached data',
                        action='store_true',
                        dest='refresh')

    args = parser.parse_args()

    loglevel_n = getattr(logging, args.loglevel.upper(), None)
    if not isinstance(loglevel_n, int):
        raise ValueError('Invalid log level: %s' % args.loglevel)
    logging.basicConfig(level=loglevel_n)

    import os
    if not os.path.exists(os.path.dirname(args.db_path)):
        raise ValueError('Check that the directory for the database file exists: {}'.format(args.db_path))

    cache = AwsCache(args.db_path)

    if args.refresh:
        cache.clear_all()

    cache.load_all()


if __name__ == '__main__':
    main()
