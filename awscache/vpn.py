import boto3

from .base import BaseResource
from .model.models import VpnConnectionModel
from .util import AwsTagsUtil


class VpnConnections(BaseResource):
    __model__ = VpnConnectionModel
    __resource_name__ = "VPN Connections"

    @staticmethod
    def list():
        client = boto3.client('ec2')
        items = client.describe_vpn_connections()['VpnConnections']
        result = []
        for item in items:
            static_routes_only = None
            if 'Options' in item:
                static_routes_only = item['Options']['StaticRoutesOnly']

            record = VpnConnectionModel(id=item['VpnConnectionId'],
                                        name=AwsTagsUtil.extract_name_from_tags(item['Tags']),
                                        state=item['State'],
                                        type=item['Type'],
                                        status=item['VgwTelemetry'][0]['Status'],
                                        last_status_change=item['VgwTelemetry'][0]['LastStatusChange'],
                                        status_message=item['VgwTelemetry'][0]['StatusMessage'],
                                        outside_ip_address=item['VgwTelemetry'][0]['OutsideIpAddress'],
                                        static_routes_only=static_routes_only,
                                        vgw_id=item['VpnGatewayId'],
                                        cgw_id=item['CustomerGatewayId'],
                                        cfn_details=AwsTagsUtil.extract_cloudformation_from_tags(item['Tags']))
            result.append(record)

        return result
