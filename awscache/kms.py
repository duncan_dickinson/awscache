from .base import BaseResource
from .model.models import KmsAliasModel, KmsModel, KmsPolicyModel


class KmsKeys(BaseResource):
    __model__ = KmsModel
    __resource_name__ = "KMS Keys"

    @staticmethod
    def list():
        import boto3

        client = boto3.client('kms')
        result = []
        for item in client.list_keys()['Keys']:
            # Get the key's metadata
            key = client.describe_key(KeyId=item['KeyArn'])['KeyMetadata']
            rotation_status = client.get_key_rotation_status(KeyId=item['KeyArn'])['KeyRotationEnabled']
            key_data = KmsModel(
                id=key['KeyId'],
                aws_account=key['AWSAccountId'],
                arn=key['Arn'],
                creation_date=key['CreationDate'],
                enabled=key['Enabled'],
                description=key['Description'],
                state=key['KeyState'],
                deletion_date=key.get('DeletionDate', None),
                valid_to=key.get('ValidTo', None),
                origin=key['Origin'],
                expiration_model=key.get('ExpirationModel'),
                key_rotation_enabled=rotation_status
            )
            result.append(key_data)
        return result


class KmsPolicies(BaseResource):
    __model__ = KmsPolicyModel
    __resource_name__ = "KMS Policies"

    @staticmethod
    def list():
        import boto3

        client = boto3.client('kms')
        result = []
        for item in client.list_keys()['Keys']:
            for policy_name in client.list_key_policies(KeyId=item['KeyId'])['PolicyNames']:
                key_policy = KmsPolicyModel(
                    key_id=item['KeyId'],
                    name=policy_name,
                    policy=client.get_key_policy(KeyId=item['KeyArn'],
                                                 PolicyName=policy_name)['Policy'])
                result.append(key_policy)
        return result


class KmsAliases(BaseResource):
    __model__ = KmsAliasModel
    __resource_name__ = "KMS Aliases"

    @staticmethod
    def list():
        import boto3

        client = boto3.client('kms')
        list = client.list_aliases()['Aliases']
        result = []
        for item in list:
            alias = KmsAliasModel(id=item['AliasArn'],
                                  name=item['AliasName'],
                                  key_id=item.get('TargetKeyId', None))
            result.append(alias)
        return result
