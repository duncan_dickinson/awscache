import logging

from sqlalchemy.orm import sessionmaker

# noinspection PyUnresolvedReferences
from .model import models
from .model.base import sqlalchemy_base


class AwsCacheData():
    """Holds the various datapoints from AWS"""

    def __init__(self):
        self.vpcs = None
        self.subnets = None
        self.internet_gateways = None
        self.customer_gateways = None
        self.vpn_gateways = None
        self.vpn_connections = None
        self.vpc_peering_connections = None
        self.vpc_endpoints = None
        self.route_tables = None
        self.flow_logs = None
        self.nacls = None

        self.nat_gateways = None
        self.elastic_ips = None

        self.kms_keys = None
        self.kms_aliases = None

        self.cfn_stacks = None
        self.cfn_export_list = None

        self.iam_groups = None
        self.iam_users = None


class AwsCache:
    """Provides a basic (and incomplete) cache for data returned from AWS API calls

    The focus is to provide data to short-run processes such as a reporting tool
    The system uses SQLAlchemy to store data in an SQLite database.

    For most usage, you just need to do something like::

        cache = AwsCache('/some/path/to/my/cache.db')
        cache.load_all()

    I'd suggest that you just call `load_all()` and let the system make all of its
    queries - that way you get new ones as they are developed.

    If you don't want to `load_all()`, you'll need to::

        cache = AwsCache('/some/path/to/my/cache.db')
        cache.create_session()

    For most cases, you can let your app run out and the connection/session will be closed.
    However, you can do this manually if you need::

        cache.dispose()

    Note that the `__del()__` method will do this for you as well.

    The system isn't overly smart and it's really a one-shot cache. Once you've run the `load_all`
    or various individual `load_` calls, subsequent calls won't use the API and will just grab
    from the database. However, if no data is found in the cache, an API call will be made. This
    means that, if you don't have a VPC, the cache will try the AWS API each time.

    If you want to "reset" the cache, call `clear_all()` and start again.
    """

    def __init__(self, db_file: str):
        """Prepares the cache (SQLAlchemy) engine

        Note that, to connect to the database, you need to `create_session()`

        :param db_file: The path and filename of the SQLite database file (e.g. /tmp/cache.db)
        """
        from sqlalchemy import create_engine
        self._sqlalchemy_uri = "sqlite:///{}".format(db_file)
        self._alchemy_engine = create_engine(self._sqlalchemy_uri, echo=False)
        self._alchemy_session = None
        self.data = AwsCacheData()

    def get_sqlalchemyuri(self) -> str:
        """
        :return: The connection string (e.g. sqlite:////tmp/cache.db)
        """
        return self._sqlalchemy_uri

    def connect(self) -> None:
        """Connects to the cache

        You'll still need to `create_session()` to get things going
        """
        if self._alchemy_engine is not None:
            self._alchemy_engine.connect()
            logging.info('Using database at: {}'.format(self._sqlalchemy_uri))

    def create_session(self) -> None:
        """Creates a database session - also connects if you haven't"""
        self.connect()
        sqlalchemy_base.metadata.create_all(self._alchemy_engine)
        self._alchemy_session = sessionmaker(bind=self._alchemy_engine)()

    def close_session(self) -> None:
        """Closes the database connection"""
        if self._alchemy_session is not None:
            self._alchemy_session.close()
            self._alchemy_session = None

    def dispose(self) -> None:
        """Disposes of the SQLAlchemy engine"""
        self.close_session()
        if self._alchemy_engine is not None:
            self._alchemy_engine.dispose()
            self._alchemy_engine = None

    def __del__(self):
        self.dispose()

    def load_all(self) -> None:
        """Utility function to run all queries and prep the cache"""

        self.connect()
        self.create_session()

        self.load_ec2_data()
        self.load_vpc_data()
        self.load_cfn_data()
        self.load_iam_data()
        self.load_kms_data()

    def clear_all(self) -> None:
        """Clears the cache of all data"""
        logging.info('Clearing the cache')
        self.connect()
        self.create_session()
        sqlalchemy_base.metadata.drop_all(self._alchemy_engine)

    def load_vpc_data(self) -> None:
        """Loads all data points related to VPCs"""
        from .vpc import Vpcs
        from .subnet import Subnets
        from .internet_gateway import InternetGateways
        from .customer_gateway import CustomerGateways
        from .vpn import VpnConnections
        from .vpn_gateway import VpnGateways
        from .vpc_peering import VpcPeerings
        from .vpc_endpoint import VpcEndpoints
        from .route_table import RouteTables
        from .vpc_flow_log import VpcFlowLogs
        from .nacl import Nacls

        self.data.vpcs = Vpcs(self._alchemy_session).load()
        self.data.subnets = Subnets(self._alchemy_session).load()
        self.data.internet_gateways = InternetGateways(self._alchemy_session).load()
        self.data.customer_gateways = CustomerGateways(self._alchemy_session).load()
        self.data.vpn_gateways = VpnGateways(self._alchemy_session).load()
        self.data.vpn_connections = VpnConnections(self._alchemy_session).load()
        self.data.vpc_peering_connections = VpcPeerings(self._alchemy_session).load()
        self.data.vpc_endpoints = VpcEndpoints(self._alchemy_session).load()
        self.data.route_tables = RouteTables(self._alchemy_session).load()
        self.data.flow_logs = VpcFlowLogs(self._alchemy_session).load()
        self.data.nacls = Nacls(self._alchemy_session).load()

    def load_ec2_data(self) -> None:
        """Loads all data points related to EC2 resources"""
        from .nat_gateway import NatGateways
        from .elasticip import ElasticIps
        self.data.nat_gateways = NatGateways(self._alchemy_session).load()
        self.data.elastic_ips = ElasticIps(self._alchemy_session).load()

    def load_kms_data(self):
        """Loads all data points related to KMS"""
        from .kms import KmsKeys, KmsAliases, KmsPolicies

        self.data.kms_keys = KmsKeys(self._alchemy_session).load()
        self.data.kms_aliases = KmsAliases(self._alchemy_session).load()
        self.data.kms_policies = KmsPolicies(self._alchemy_session).load()

    def load_cfn_data(self) -> None:
        """Loads all data points related to Cloud Formation"""
        from .cloud_formation import CloudFormationStacks, CloudFormationExports

        self.data.cfn_stacks = CloudFormationStacks(self._alchemy_session).load()
        self.data.cfn_export_list = CloudFormationExports(self._alchemy_session).load()

    def load_iam_data(self) -> None:
        """Loads all data points related to IAM"""
        from .iam_users import IamUsers
        from .iam_groups import IamGroups
        self.data.iam_users = IamUsers(self._alchemy_session).load()
        self.data.iam_groups = IamGroups(self._alchemy_session).load()
