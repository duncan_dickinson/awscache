import boto3

from .base import BaseResource
from .model.models import NatGatewayModel


class NatGateways(BaseResource):
    __model__ = NatGatewayModel
    __resource_name__ = "NAT Gateways"

    @staticmethod
    def list():
        client = boto3.client('ec2')
        result = []
        listing_response = client.describe_nat_gateways()
        while True:
            for item in listing_response['NatGateways']:
                result.append(
                    NatGatewayModel(
                        id=item['NatGatewayId'],
                        vpc_id=item['VpcId'],
                        subnet_id=item['SubnetId'],
                        create_date=item['CreateTime'],
                        delete_date=item.get('DeleteTime', None),
                        state=item['State'],
                        failure_code=item.get('FailureCode', None),
                        failure_message=item.get('FailureMessage', None)
                    ))
            if 'NextToken' in listing_response:
                listing_response = client.list_groups(NextToken=listing_response['NextToken'])
            else:
                break

        return result
