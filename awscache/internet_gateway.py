import boto3

from .base import BaseResource
from .model.models import InternetGatewayModel
from .util import AwsTagsUtil


class InternetGateways(BaseResource):
    __model__ = InternetGatewayModel
    __resource_name__ = "Internet Gateways"

    @staticmethod
    def list():
        client = boto3.client('ec2')
        items = client.describe_internet_gateways()['InternetGateways']
        result = []
        for item in items:
            for attachment in item['Attachments']:
                record = InternetGatewayModel(id=item['InternetGatewayId'],
                                              name=AwsTagsUtil.extract_name_from_tags(item['Tags']),
                                              vpc_id=attachment['VpcId'],
                                              state=attachment['State'],
                                              cfn_details=AwsTagsUtil.extract_cloudformation_from_tags(item['Tags']))
                result.append(record)

        return result
