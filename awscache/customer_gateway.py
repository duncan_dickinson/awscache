import boto3

from .base import BaseResource
from .model.models import CustomerGatewayModel
from .util import AwsTagsUtil


class CustomerGateways(BaseResource):
    __model__ = CustomerGatewayModel
    __resource_name__ = "Customer Gateways"

    @staticmethod
    def list():
        client = boto3.client('ec2')
        items = client.describe_customer_gateways()['CustomerGateways']
        result = []
        for item in items:
            record = CustomerGatewayModel(id=item['CustomerGatewayId'],
                                          name=AwsTagsUtil.extract_name_from_tags(item['Tags']),
                                          state=item['State'],
                                          type=item['Type'],
                                          ip_address=item['IpAddress'],
                                          bgp_asn=item['BgpAsn'],
                                          cfn_details=AwsTagsUtil.extract_cloudformation_from_tags(item['Tags']))
            result.append(record)

        return result
