import boto3

from .base import BaseResource
from .model.models import VpcFlowLogModel


class VpcFlowLogs(BaseResource):
    __model__ = VpcFlowLogModel
    __resource_name__ = "VPC Flow Logs"

    @staticmethod
    def list():
        client = boto3.client('ec2')
        item_listing = client.describe_flow_logs()
        result = []

        while True:
            for item in item_listing['FlowLogs']:
                result.append(VpcFlowLogModel(
                    id=item['FlowLogId'],
                    creation_time=item['CreationTime'],
                    status=item['FlowLogStatus'],
                    resource_id=item['ResourceId'],
                    traffic_type=item['TrafficType'],
                    log_group_name=item['LogGroupName'],
                    deliver_logs_status=item.get('DeliverLogsStatus', None),
                    deliver_logs_error_message=item.get('DeliverLogsErrorMessage', None),
                    deliver_logs_permission_arn=item['DeliverLogsPermissionArn']
                ))

            if 'NextToken' in item_listing:
                item_listing = client.describe_flow_logs(NextToken=item_listing['NextToken'])
            else:
                break

        return result
