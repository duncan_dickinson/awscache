import pprint

import boto3

from awscache.model.models import CloudFormationExportModel, CloudFormationStackModel
from .base import BaseResource


class CloudFormationStacks(BaseResource):
    __model__ = CloudFormationStackModel
    __resource_name__ = "CloudFormation Stacks"

    @staticmethod
    def list():
        client = boto3.client('cloudformation')
        items = client.describe_stacks()['Stacks']
        result = []
        for item in items:
            template = pprint.pformat(client.get_template(StackName=item['StackName'])['TemplateBody'])
            record = CloudFormationStackModel(item['StackId'],
                                              item['StackName'],
                                              item['Description'],
                                              item['CreationTime'],
                                              item.get('LastUpdatedTime', None),
                                              item['StackStatus'],
                                              item.get('StackStatusReason', None),
                                              template=template)
            result.append(record)
        return result


class CloudFormationExports(BaseResource):
    __model__ = CloudFormationExportModel
    __resource_name__ = "CloudFormation Exports"

    @staticmethod
    def list():
        client = boto3.client('cloudformation')
        items = client.list_exports()['Exports']
        result = []
        for item in items:
            record = CloudFormationExportModel(item['ExportingStackId'], item['Name'], item['Value'])
            result.append(record)
        return result
