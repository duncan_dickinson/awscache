import boto3

from .base import BaseResource
from .model.models import VpnGatewayModel
from .util import AwsTagsUtil


class VpnGateways(BaseResource):
    __model__ = VpnGatewayModel
    __resource_name__ = "VPN Gateways"

    @staticmethod
    def list():
        client = boto3.client('ec2')
        items = client.describe_vpn_gateways()['VpnGateways']
        result = []
        for item in items:
            for attachment in item['VpcAttachments']:
                record = VpnGatewayModel(id=item['VpnGatewayId'],
                                         name=AwsTagsUtil.extract_name_from_tags(item['Tags']),
                                         state=item['State'],
                                         type=item['Type'],
                                         vpc_id=attachment['VpcId'],
                                         vpc_state=attachment['State'],
                                         cfn_details=AwsTagsUtil.extract_cloudformation_from_tags(item['Tags']))
                result.append(record)

        return result
