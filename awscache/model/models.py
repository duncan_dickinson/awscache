from sqlalchemy import Boolean, Column, DateTime, ForeignKey, Integer, LargeBinary, String
from sqlalchemy.orm import relationship

from .base import AwsBaseCfnModel, AwsBaseModel, sqlalchemy_base


class GatewayModel(AwsBaseCfnModel):
    __tablename__ = 'gateway'

    gw_type = Column(String, nullable=False)

    route_table_entry = relationship('RouteTableEntryModel',
                                     back_populates='gateway')

    __mapper_args__ = {
        'polymorphic_identity': 'gateway',
        'polymorphic_on': gw_type
    }

    def __init__(self, id, name=None, cfn_details=None):
        AwsBaseCfnModel.__init__(self, id, name, cfn_details=cfn_details)


class VpcModel(AwsBaseCfnModel):
    __tablename__ = 'vpc'

    cidr_block = Column(String(20), nullable=False)
    enable_dns_support = Column(Boolean)
    enable_dns_hostnames = Column(Boolean)

    subnets = relationship('SubnetModel',
                           back_populates='vpc')

    internet_gateways = relationship('InternetGatewayModel',
                                     back_populates='vpc')

    vpn_gateways = relationship('VpnGatewayModel',
                                back_populates='vpc')

    route_tables = relationship('RouteTableModel',
                                back_populates='vpc')

    endpoints = relationship('VpcEndpointModel',
                             back_populates='vpc')

    nat_gateways = relationship('NatGatewayModel',
                                back_populates='vpc')

    flow_logs = relationship('VpcFlowLogModel',
                             back_populates='vpc')

    nacls = relationship('NaclModel',
                         back_populates='vpc')

    def __init__(self, id, name, cidr_block,
                 enable_dns_support, enable_dns_hostnames,
                 cfn_details=None):
        AwsBaseCfnModel.__init__(self, id, name, cfn_details=cfn_details)
        self.cidr_block = cidr_block
        self.enable_dns_support = enable_dns_support
        self.enable_dns_hostnames = enable_dns_hostnames

    __mapper_args__ = {
        'concrete': True
    }


class CloudFormationStackModel(AwsBaseModel):
    __tablename__ = 'cloudformation_stack'

    description = Column(String, nullable=True)
    creation_time = Column(DateTime, nullable=False)
    last_updated_time = Column(DateTime, nullable=True)
    status = Column(String, nullable=False)
    status_reason = Column(String, nullable=True)
    template = Column(LargeBinary)

    exports = relationship("CloudFormationExportModel", back_populates="stack")

    def __init__(self, id, name, description, creation_time, last_updated_time, status,
                 status_reason, template=None):
        AwsBaseModel.__init__(self, id, name)
        self.description = description
        self.creation_time = creation_time
        self.last_updated_time = last_updated_time
        self.status = status
        self.status_reason = status_reason
        self.template = None  # template.decode('string_escape')[1:-1]

    __mapper_args__ = {
        'concrete': True
    }


class CloudFormationExportModel(sqlalchemy_base):
    __tablename__ = 'cloudformation_export'

    name = Column(String, primary_key=True)
    value = Column(String)
    stack_id = Column(String, ForeignKey('cloudformation_stack.id'), primary_key=True)

    stack = relationship("CloudFormationStackModel", back_populates="exports")

    def __init__(self, stack_id, name, value):
        self.stack_id = stack_id
        self.name = name
        self.value = value


class CustomerGatewayModel(GatewayModel):
    id = Column(String, ForeignKey('gateway.id'), primary_key=True)
    __tablename__ = 'cgw'
    __mapper_args__ = {
        'polymorphic_identity': 'cgw'
    }

    state = Column(String)
    type = Column(String)
    ip_address = Column(String)
    bgp_asn = Column(String)

    vpn = relationship('VpnConnectionModel',
                       back_populates='cgw')

    def __init__(self, id, name,
                 state, type, ip_address, bgp_asn,
                 cfn_details=None):
        GatewayModel.__init__(self, id, name, cfn_details=cfn_details)
        self.id = id
        self.state = state
        self.type = type
        self.ip_address = ip_address
        self.bgp_asn = bgp_asn
        self.gateway_id = id


class ElasticIpModel(AwsBaseModel):
    __tablename__ = 'elastic_ip'

    allocation_id = Column(String)
    public_ip = Column(String)
    private_ip = Column(String)
    network_interface_id = Column(String)

    domain = Column(String)

    instance_id = Column(String)
    association_id = Column(String)

    def __init__(self, id, network_interface_owner_id, domain,
                 allocation_id, public_ip, private_ip, network_interface_id,
                 instance_id, association_id):
        AwsBaseModel.__init__(self, id=id, aws_account=network_interface_owner_id)
        self.domain = domain
        self.allocation_id = allocation_id
        self.public_ip = public_ip
        self.private_ip = private_ip
        self.network_interface_id = network_interface_id
        self.instance_id = instance_id
        self.association_id = association_id


class IamGroupMembershipModel(sqlalchemy_base):
    __tablename__ = 'iam_group_membership'

    user_id = Column(String, ForeignKey('iam_user.id'), primary_key=True)
    group_id = Column(String, ForeignKey('iam_group.id'), primary_key=True)

    def __init__(self, user_id, group_id):
        self.user_id = user_id
        self.group_id = group_id


class IamGroupModel(AwsBaseModel):
    __tablename__ = 'iam_group'

    users = relationship("IamUserModel",
                         secondary="iam_group_membership",
                         back_populates="groups")

    path = Column(String, nullable=False)
    create_date = Column(DateTime, nullable=False)

    def __init__(self, id, name, arn, path, create_date):
        AwsBaseModel.__init__(self, id, name=name, arn=arn)
        self.path = path
        self.create_date = create_date


class IamUserModel(AwsBaseModel):
    __tablename__ = 'iam_user'

    access_keys = relationship("IamUserAccessKeyModel", back_populates="user")
    mfa_devices = relationship("IamUserMfaDeviceModel", back_populates="user")

    groups = relationship("IamGroupModel",
                          secondary="iam_group_membership",
                          back_populates="users")

    path = Column(String, nullable=False)
    create_date = Column(DateTime, nullable=False)
    login_profile_configured = Column(Boolean)
    password_last_used = Column(DateTime)
    password_creation_date = Column(DateTime)
    password_reset_required = Column(Boolean)

    def __init__(self, id, name, arn, path, create_date, login_profile_configured,
                 password_last_used, password_creation_date, password_reset_required):
        AwsBaseModel.__init__(self, id, name, arn)
        self.path = path
        self.create_date = create_date
        self.login_profile_configured = login_profile_configured
        self.password_last_used = password_last_used
        self.password_creation_date = password_creation_date
        self.password_reset_required = password_reset_required


class IamUserAccessKeyModel(AwsBaseModel):
    __tablename__ = 'iam_user_access_keys'

    user = relationship("IamUserModel", back_populates="access_keys")

    user_id = Column(String, ForeignKey('iam_user.id'))
    create_date = Column(DateTime, nullable=False)
    last_used_date = Column(DateTime)
    status = Column(String)

    def __init__(self, id, user_id, create_date, last_used_date, status):
        AwsBaseModel.__init__(self, id)
        self.user_id = user_id
        self.create_date = create_date
        self.last_used_date = last_used_date
        self.status = status


class IamUserMfaDeviceModel(sqlalchemy_base):
    __tablename__ = 'iam_user_mfa'

    user = relationship("IamUserModel", back_populates="mfa_devices")
    user_id = Column(String, ForeignKey('iam_user.id'))
    serial_number = Column(String, primary_key=True)
    enable_date = Column(String)

    def __init__(self, user_id, serial_number, enable_date):
        self.user_id = user_id
        self.serial_number = serial_number
        self.enable_date = enable_date


class InternetGatewayModel(GatewayModel):
    id = Column(String, ForeignKey('gateway.id'), primary_key=True)
    __tablename__ = 'igw'
    __mapper_args__ = {
        'polymorphic_identity': 'igw'
    }

    state = Column(String)

    vpc_id = Column(String, ForeignKey('vpc.id'))
    vpc = relationship('VpcModel',
                       back_populates='internet_gateways')

    def __init__(self, id, name, vpc_id, state, cfn_details=None):
        GatewayModel.__init__(self, id, name, cfn_details=cfn_details)
        self.id = id
        self.vpc_id = vpc_id
        self.state = state
        self.gateway_id = id


class KmsPolicyModel(sqlalchemy_base):
    from sqlalchemy import Column, ForeignKey, String
    from sqlalchemy.orm import relationship

    __tablename__ = 'kms_policy'

    key_id = Column(String, ForeignKey('kms.id'), primary_key=True)
    name = Column(String, primary_key=True)
    policy = Column(String, nullable=False)

    key = relationship("KmsModel", back_populates="policies")

    def __init__(self, key_id, name, policy):
        self.key_id = key_id
        self.name = name
        self.policy = policy


class KmsModel(AwsBaseModel):
    from sqlalchemy import Boolean, Column, DateTime, String
    from sqlalchemy.orm import relationship

    __tablename__ = 'kms'

    creation_date = Column(DateTime)
    enabled = Column(Boolean)
    state = Column(String)
    deletion_date = Column(DateTime)
    valid_to = Column(DateTime)
    origin = Column(String)
    expiration_model = Column(String)
    policy = Column(String)
    key_rotation_enabled = Column(Boolean)

    aliases = relationship("KmsAliasModel", back_populates="key")
    policies = relationship("KmsPolicyModel", back_populates="key")

    def __init__(self, id, arn, description,
                 aws_account, creation_date, enabled,
                 state, deletion_date, valid_to, origin, expiration_model,
                 key_rotation_enabled,
                 cfn_details=None):
        AwsBaseModel.__init__(self, id,
                              arn=arn,
                              description=description,
                              aws_account=aws_account)
        self.creation_date = creation_date
        self.enabled = enabled
        self.state = state
        self.deletion_date = deletion_date
        self.valid_to = valid_to
        self.origin = origin
        self.expiration_model = expiration_model
        self.key_rotation_enabled = key_rotation_enabled


class KmsAliasModel(AwsBaseModel):
    from sqlalchemy import Column, ForeignKey, String
    from sqlalchemy.orm import relationship

    __tablename__ = 'kms_alias'

    key_id = Column(String, ForeignKey('kms.id'))

    key = relationship("KmsModel", back_populates="aliases")

    def __init__(self, id, name, key_id):
        AwsBaseModel.__init__(self, id, name)
        self.key_id = key_id


class NaclModel(AwsBaseCfnModel):
    __tablename__ = 'nacl'

    vpc_id = Column(String, ForeignKey('vpc.id'))
    vpc = relationship('VpcModel',
                       back_populates="nacls")

    nacl_entries = relationship('NaclEntryModel',
                                back_populates="nacl")

    associations = relationship('NaclAssociationModel',
                                back_populates='nacl')

    default = Column(Boolean)

    def __init__(self, id,
                 vpc_id, default,
                 name=None, cfn_details=None, tags=None):
        AwsBaseCfnModel.__init__(self, id=id, name=name, cfn_details=cfn_details, tags=tags)
        self.vpc_id = vpc_id
        self.default = default


class NaclAssociationModel(AwsBaseCfnModel):
    __tablename__ = 'nacl_association'

    nacl_id = Column(String, ForeignKey('nacl.id'))
    nacl = relationship('NaclModel',
                        back_populates='associations')

    subnet_id = Column(String, ForeignKey('subnet.id'))
    subnet = relationship('SubnetModel',
                          back_populates='nacl_association')

    def __init__(self, id, nacl_id, subnet_id):
        AwsBaseCfnModel.__init__(self, id=id)
        self.nacl_id = nacl_id
        self.subnet_id = subnet_id


class NaclEntryModel(sqlalchemy_base):
    __tablename__ = 'nacl_entry'

    nacl_id = Column(String, ForeignKey('nacl.id'), primary_key=True)
    nacl = relationship('NaclModel',
                        back_populates="nacl_entries")

    index = Column(Integer, primary_key=True)

    rule_number = Column(Integer)
    protocol = Column(String)
    rule_action = Column(String)
    egress = Column(Boolean)
    cidr_block = Column(String)
    ipv6_cidr_block = Column(String)

    icmp_type = Column(Integer)
    icmp_code = Column(Integer)

    port_range_from = Column(Integer)
    port_range_to = Column(Integer)

    def __init__(self, nacl_id, index, rule_number, protocol, rule_action, egress,
                 cidr_block, ipv6_cidr_block, icmp_type, icmp_code,
                 port_range_from, port_range_to):
        self.nacl_id = nacl_id
        self.index = index
        self.rule_number = rule_number
        self.protocol = protocol
        self.rule_action = rule_action
        self.egress = egress
        self.cidr_block = cidr_block
        self.ipv6_cidr_block = ipv6_cidr_block
        self.icmp_type = icmp_type
        self.icmp_code = icmp_code
        self.port_range_from = port_range_from
        self.port_range_to = port_range_to


class NatGatewayModel(AwsBaseCfnModel):
    __tablename__ = 'nat_gateway'

    vpc_id = Column(String, ForeignKey('vpc.id'))
    vpc = relationship('VpcModel',
                       back_populates='nat_gateways')

    subnet_id = Column(String, ForeignKey('subnet.id'))
    subnet = relationship('SubnetModel',
                          back_populates='nat_gateways')

    route_table_entry = relationship('RouteTableEntryModel',
                                     back_populates='nat_gateway')

    # addresses = relationship(NatGatewayAddressesModel, back_populates=NatGatewayAddressesModel.nat_gateway)

    create_date = Column(DateTime)
    delete_date = Column(DateTime)

    state = Column(String)
    failure_code = Column(String)
    failure_message = Column(String)

    def __init__(self, id, vpc_id, subnet_id,
                 create_date, delete_date, state, failure_code, failure_message,
                 name=None, cfn_details=None):
        AwsBaseCfnModel.__init__(self, id, name, cfn_details=cfn_details)
        self.vpc_id = vpc_id
        self.subnet_id = subnet_id
        self.create_date = create_date
        self.delete_date = delete_date
        self.state = state
        self.failure_code = failure_code
        self.failure_message = failure_message

        # class NatGatewayAddressesModel(AwsBaseModel):
        #     __tablename__ = 'nat_gateway_address'
        #
        #     nat_gateway_id = Column(String, ForeignKey(NatGatewayModel.id))
        #     nat_gateway = relationship(NatGatewayModel, back_populates=NatGatewayModel.addresses)
        #
        #     allocation_id = Column(String)
        #     public_ip = Column(String)
        #     private_ip = Column(String)
        #     network_interface_id = Column(String)
        #
        #     def __init__(self, allocation_id, public_ip, private_ip, network_interface_id):
        #         AwsBaseBaseModel.__init__(self, id, name, cfn_details=cfn_details)


class RouteTableModel(AwsBaseCfnModel):
    __tablename__ = 'route_table'

    vpc_id = Column(String, ForeignKey('vpc.id'))
    vpc = relationship('VpcModel',
                       back_populates="route_tables")

    routes = relationship('RouteTableEntryModel',
                          back_populates="route_table")

    associations = relationship('RouteTableAssociationModel',
                                back_populates='route_table')

    def __init__(self, id, name,
                 vpc_id,
                 cfn_details=None, tags=None):
        AwsBaseCfnModel.__init__(self, id, name, cfn_details=cfn_details, tags=tags)
        self.vpc_id = vpc_id


class RouteTableAssociationModel(AwsBaseCfnModel):
    __tablename__ = 'route_table_association'

    route_table_id = Column(String, ForeignKey('route_table.id'))
    route_table = relationship('RouteTableModel',
                               back_populates='associations')

    subnet_id = Column(String, ForeignKey('subnet.id'))
    subnet = relationship('SubnetModel',
                          back_populates='route_table_association')

    main = Column(Boolean)

    def __init__(self, id, route_table_id, subnet_id, main):
        AwsBaseCfnModel.__init__(self, id=id)
        self.route_table_id = route_table_id
        self.subnet_id = subnet_id
        self.main = main


class RouteTableEntryModel(sqlalchemy_base):
    __tablename__ = 'route_table_entry'

    index = Column(Integer, primary_key=True)

    route_table = relationship('RouteTableModel',
                               back_populates='routes')
    route_table_id = Column(String, ForeignKey(RouteTableModel.id),
                            primary_key=True)

    destination_cidr_block = Column(String)
    destination_prefix_list_id = Column(String)

    gateway_id = Column(String, ForeignKey('gateway.id'))
    gateway = relationship('GatewayModel',
                           back_populates='route_table_entry')

    vpc_peering_connection_id = Column(String, ForeignKey('vpc_peering.id'))
    # vpc_peering_connection = relationship('VpcPeeringConnectionModel')

    instance_id = Column(String)
    instance_owner_id = Column(String)
    network_interface_id = Column(String)

    nat_gateway_id = Column(String, ForeignKey('nat_gateway.id'))
    nat_gateway = relationship('NatGatewayModel',
                               back_populates='route_table_entry')

    state = Column(String)
    origin = Column(String)

    destination_ipv6_cidr_block = Column(String)
    egress_only_igw_id = Column(String)

    def __init__(self, index, route_table_id,
                 state, origin,
                 destination_cidr_block=None, destination_prefix_list_id=None,
                 instance_id=None, instance_owner_id=None, network_interface_id=None,
                 gateway_id=None, nat_gateway_id=None, vpc_peering_connection_id=None,
                 destination_ipv6_cidr_block=None, egress_only_igw_id=None):
        self.index = index
        self.route_table_id = route_table_id

        self.state = state
        self.origin = origin

        self.destination_cidr_block = destination_cidr_block
        self.destination_prefix_list_id = destination_prefix_list_id

        self.destination_ipv6_cidr_block = destination_ipv6_cidr_block
        self.egress_only_igw_id = egress_only_igw_id

        self.gateway_id = gateway_id
        self.nat_gateway_id = nat_gateway_id

        self.vpc_peering_connection_id = vpc_peering_connection_id
        self.instance_id = instance_id
        self.instance_owner_id = instance_owner_id
        self.network_interface_id = network_interface_id


class SubnetModel(AwsBaseCfnModel):
    __tablename__ = 'subnet'

    cidr_block = Column(String(20), nullable=False)
    availability_zone = Column(String)
    state = Column(String)
    map_public_ip_on_launch = Column(Boolean)
    default_for_az = Column(Boolean)
    available_ip_address_count = Column(Integer)
    assign_ipv6_address_on_creation = Column(Boolean)

    vpc_id = Column(String, ForeignKey(VpcModel.__tablename__ + '.id'))
    vpc = relationship('VpcModel',
                       back_populates="subnets")

    flow_logs = relationship('VpcFlowLogModel',
                             back_populates='subnet')

    route_table_association = relationship('RouteTableAssociationModel',
                                           back_populates='subnet')

    nacl_association = relationship('NaclAssociationModel',
                                    back_populates='subnet')

    nat_gateways = relationship('NatGatewayModel',
                                back_populates='subnet')

    def __init__(self, id, name, vpc_id, cidr_block, availability_zone,
                 state, map_public_ip_on_launch, default_for_az,
                 available_ip_address_count, assign_ipv6_address_on_creation,
                 cfn_details=None):
        AwsBaseCfnModel.__init__(self, id, name, cfn_details=cfn_details)
        self.cidr_block = cidr_block
        self.availability_zone = availability_zone
        self.state = state
        self.map_public_ip_on_launch = map_public_ip_on_launch
        self.default_for_az = default_for_az
        self.available_ip_address_count = available_ip_address_count
        self.assign_ipv6_address_on_creation = assign_ipv6_address_on_creation
        self.vpc_id = vpc_id


class VpcEndpointModel(GatewayModel):
    id = Column(String, ForeignKey('gateway.id'), primary_key=True)
    __tablename__ = 'vpc_endpoint'
    __mapper_args__ = {
        'polymorphic_identity': 'vpc_endpoint'
    }

    vpc_id = Column(String, ForeignKey('vpc.id'))
    vpc = relationship('VpcModel',
                       back_populates='endpoints')

    state = Column(String)
    creation_timestamp = Column(DateTime)
    service_name = Column(String)
    policy = Column(String)

    def __init__(self, id, vpc_id, state, creation_timestamp, service_name, policy):
        GatewayModel.__init__(self, id=id)
        self.id = id
        self.vpc_id = vpc_id
        self.state = state
        self.creation_timestamp = creation_timestamp
        self.service_name = service_name
        self.policy = policy
        self.gateway_id = id


class VpcFlowLogModel(AwsBaseCfnModel):
    __tablename__ = 'flow_log'

    creation_time = Column(DateTime)
    status = Column(String)
    resource_id = Column(String)

    vpc_id = Column(String, ForeignKey('vpc.id'))
    vpc = relationship('VpcModel',
                       back_populates="flow_logs")

    subnet_id = Column(String, ForeignKey('subnet.id'))
    subnet = relationship('SubnetModel',
                          back_populates="flow_logs")

    traffic_type = Column(String)
    log_group_name = Column(String)
    deliver_logs_status = Column(String)
    deliver_logs_error_message = Column(String)
    deliver_logs_permission_arn = Column(String)

    def __init__(self, id, creation_time, status, resource_id, traffic_type,
                 log_group_name, deliver_logs_status, deliver_logs_error_message,
                 deliver_logs_permission_arn):
        AwsBaseCfnModel.__init__(self, id)
        self.creation_time = creation_time
        self.status = status
        self.resource_id = resource_id

        if resource_id.startswith('vpc-'):
            self.vpc_id = resource_id
        elif resource_id.startswith('subnet-'):
            self.subnet_id = resource_id

        self.traffic_type = traffic_type
        self.log_group_name = log_group_name
        self.deliver_logs_status = deliver_logs_status
        self.deliver_logs_error_message = deliver_logs_error_message
        self.deliver_logs_permission_arn = deliver_logs_permission_arn


class VpcPeeringConnectionModel(GatewayModel):
    id = Column(String, ForeignKey('gateway.id'), primary_key=True)

    __tablename__ = 'vpc_peering'
    __mapper_args__ = {
        'polymorphic_identity': 'vpc_peering'
    }

    accepter_vpc_id = Column(String, ForeignKey('vpc.id'))
    accepter_vpc_allow_dns_resolution_from_remote_vpc = Column(Boolean)

    requester_vpc_id = Column(String, ForeignKey('vpc.id'))
    requester_vpc_allow_dns_resolution_from_remote_vpc = Column(Boolean)

    # route_table_entry = relationship('RouteTableEntryModel',
    #                                 back_populates='vpc_peering_connection')

    expiration_time = Column(DateTime)
    status_code = Column(String)
    status_message = Column(String)

    def __init__(self, id, name,
                 accepter_vpc_id, accepter_vpc_allow_dns_resolution_from_remote_vpc,
                 requester_vpc_id, requester_vpc_allow_dns_resolution_from_remote_vpc,
                 expiration_time, status_code, status_message,
                 cfn_details=None):
        GatewayModel.__init__(self, id=id, name=name, cfn_details=cfn_details)
        self.id = id
        self.accepter_vpc_id = accepter_vpc_id
        self.accepter_vpc_allow_dns_resolution_from_remote_vpc = accepter_vpc_allow_dns_resolution_from_remote_vpc
        self.requester_vpc_id = requester_vpc_id
        self.requester_vpc_allow_dns_resolution_from_remote_vpc = requester_vpc_allow_dns_resolution_from_remote_vpc
        self.expiration_time = expiration_time
        self.status_code = status_code
        self.status_message = status_message
        self.gateway_id = id


class VpnConnectionModel(AwsBaseCfnModel):
    __tablename__ = 'vpn'

    state = Column(String)
    type = Column(String)
    status = Column(String)
    last_status_change = Column(DateTime)
    status_message = Column(String)
    outside_ip_address = Column(String)
    static_routes_only = Column(Boolean)

    cgw_id = Column(String, ForeignKey('cgw.id'))
    cgw = relationship('CustomerGatewayModel',
                       back_populates="vpn")

    vgw_id = Column(String, ForeignKey('vgw.id'))
    vgw = relationship('VpnGatewayModel',
                       back_populates="vpn")

    def __init__(self, id, name,
                 state, type, cgw_id, vgw_id,
                 status, last_status_change, status_message,
                 outside_ip_address, static_routes_only,
                 cfn_details=None):
        AwsBaseCfnModel.__init__(self, id, name, cfn_details=cfn_details)
        self.type = type
        self.state = state
        self.cgw_id = cgw_id
        self.vgw_id = vgw_id
        self.status = status
        self.last_status_change = last_status_change
        self.status_message = status_message
        self.outside_ip_address = outside_ip_address
        self.static_routes_only = static_routes_only


class VpnGatewayModel(GatewayModel):
    id = Column(String, ForeignKey('gateway.id'), primary_key=True)
    __tablename__ = 'vgw'
    __mapper_args__ = {
        'polymorphic_identity': 'vgw'
    }

    state = Column(String)
    type = Column(String)

    vpc_id = Column(String, ForeignKey('vpc.id'))
    vpc_state = Column(String)
    vpc = relationship('VpcModel',
                       back_populates="vpn_gateways")

    vpn = relationship('VpnConnectionModel',
                       back_populates="vgw")

    def __init__(self, id, name,
                 state, type, vpc_id, vpc_state,
                 cfn_details=None):
        GatewayModel.__init__(self, id, name, cfn_details=cfn_details)
        self.id = id
        self.state = state
        self.type = type
        self.vpc_id = vpc_id
        self.vpc_state = vpc_state
        self.gateway_id = id
