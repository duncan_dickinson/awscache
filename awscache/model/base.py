from sqlalchemy import Column, DateTime, String
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.sql.functions import current_timestamp

from ..util import AwsConnectionDetails, AwsTagsUtil

sqlalchemy_base = declarative_base()


class AwsBaseModel(sqlalchemy_base):
    __abstract__ = True

    id = Column(String(), primary_key=True)
    arn = Column(String, nullable=True)
    date_created = Column(DateTime, default=current_timestamp())
    date_modified = Column(DateTime, default=current_timestamp(),
                           onupdate=current_timestamp())
    name = Column(String, nullable=True)
    description = Column(String, nullable=True)
    aws_region = Column(String, nullable=True)
    aws_account = Column(String, nullable=True)

    def __init__(self, id, name=None, arn=None, description=None, aws_account=None, aws_region=None, tags=None):
        aws_conn = AwsConnectionDetails()
        self.id = id
        self.arn = arn
        self.name = name
        self.aws_account = aws_account if aws_account else aws_conn.get_account()
        self.aws_region = aws_region if aws_region else aws_conn.current_region()
        self.description = description

        if tags:
            if not self.description:
                self.description = AwsTagsUtil.extract_item_from_tags(AwsTagsUtil.TAG_DESCRIPTION, tags)
            if not self.name:
                self.name = AwsTagsUtil.extract_name_from_tags(tags)


class AwsBaseCfnModel(AwsBaseModel):
    __abstract__ = True

    cloudformation_stack_id = Column(String, nullable=True)
    cloudformation_stack_name = Column(String, nullable=True)
    cloudformation_logical_id = Column(String, nullable=True)

    def __init__(self, id, name=None, arn=None, description=None,
                 aws_account=None, aws_region=None, cfn_details={}, tags=None):
        AwsBaseModel.__init__(self, id, name, arn, description, aws_account, aws_region, tags)
        if cfn_details:
            self.cloudformation_stack_id = cfn_details.stack_id
            self.cloudformation_stack_name = cfn_details.stack_name
            self.cloudformation_logical_id = cfn_details.logical_id
        elif tags:
            cfn = AwsTagsUtil.extract_cloudformation_from_tags(tags)
            self.cloudformation_stack_id = cfn.stack_id
            self.cloudformation_stack_name = cfn.stack_name
            self.cloudformation_logical_id = cfn.logical_id
