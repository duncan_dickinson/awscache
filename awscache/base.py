import logging
from abc import ABCMeta

from sqlalchemy.ext.declarative import DeclarativeMeta
from sqlalchemy.orm import Query


class BaseResource:
    __metaclass__ = ABCMeta
    __model__ = None
    __resource_name__ = 'None'

    def __init__(self, sqlalchemy_session: DeclarativeMeta):
        self.session = sqlalchemy_session

    def load(self):
        if not self.session.query(self.__model__).count():
            logging.info('Cache: load {}'.format(self.__resource_name__))
            for key in self.list():
                self.session.add(key)
            self.session.commit()

        return self

    def count(self) -> int:
        return self.session.query(self.__model__).count()

    def query_all(self) -> Query:
        self.load()
        return self.session.query(self.__model__).order_by(self.__model__.name)
