import boto3

from .base import BaseResource
from .model.models import VpcPeeringConnectionModel
from .util import AwsTagsUtil


class VpcPeerings(BaseResource):
    __model__ = VpcPeeringConnectionModel
    __resource_name__ = "VPC Peerings"

    @staticmethod
    def list():
        client = boto3.client('ec2')
        items = client.describe_vpc_peering_connections()['VpcPeeringConnections']
        result = []
        for item in items:
            record = VpcPeeringConnectionModel(id=item['VpcPeeringConnectionId'],
                                               name=AwsTagsUtil.extract_name_from_tags(item['Tags']),
                                               accepter_vpc_id=item['AccepterVpcInfo']['VpcId'],
                                               accepter_vpc_allow_dns_resolution_from_remote_vpc=
                                               item['AccepterVpcInfo']['PeeringOptions'][
                                                   'AllowDnsResolutionFromRemoteVpc'],
                                               requester_vpc_id=item['RequesterVpcInfo']['VpcId'],
                                               requester_vpc_allow_dns_resolution_from_remote_vpc=
                                               item['RequesterVpcInfo']['PeeringOptions'][
                                                   'AllowDnsResolutionFromRemoteVpc'],
                                               expiration_time=item.get('ExpirationTime', None),
                                               status_code=item['Status']['Code'],
                                               status_message=item['Status']['Message'],
                                               cfn_details=AwsTagsUtil.extract_cloudformation_from_tags(item['Tags']))
            result.append(record)

        return result
