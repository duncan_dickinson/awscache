import boto3
from botocore.exceptions import ClientError

from .base import BaseResource
from .model.models import IamUserAccessKeyModel, IamUserMfaDeviceModel, IamUserModel


class IamUsers(BaseResource):
    __model__ = IamUserModel
    __resource_name__ = "IAM Users"

    @staticmethod
    def list():
        """
        Compiles an array of users as well as the MFA and access keys for the user
        :return: A mixed type array of user, mfa and access key instances
        """
        client = boto3.client('iam')
        iam = boto3.resource('iam')
        user_list = []
        result = []

        login_profile_configured = False
        password_creation_date = None
        password_reset_required = None

        listing_response = client.list_users()
        while True:
            for user in listing_response['Users']:
                user_list.append(user['UserName'])

            if listing_response['IsTruncated']:
                listing_response = client.list_users(Marker=listing_response['Marker'])
            else:
                break

        for user_name in user_list:
            user = iam.User(user_name)

            try:
                profile = user.LoginProfile()
                profile.load()
                login_profile_configured = True
                password_creation_date = profile.create_date
                password_reset_required = profile.password_reset_required
            except ClientError as e:
                # Unfortunately the load will throw an exception when the user doesn't have a login profile
                # and there's not API method for check this. Sorry, I too hate program flow-by-exception too.
                pass

            result.append(IamUserModel(
                id=user.user_id,
                name=user.name,
                arn=user.arn,
                path=user.path,
                create_date=user.create_date,
                login_profile_configured=login_profile_configured,
                password_last_used=user.password_last_used,
                password_creation_date=password_creation_date,
                password_reset_required=password_reset_required
            ))

            for access_key in user.access_keys.all():
                last_used_date = client.get_access_key_last_used(AccessKeyId=access_key.access_key_id)[
                    'AccessKeyLastUsed'].get('LastUsedDate', None)
                result.append(IamUserAccessKeyModel(
                    id=access_key.access_key_id,
                    user_id=user.user_id,
                    create_date=access_key.create_date,
                    last_used_date=last_used_date,
                    status=access_key.status
                ))

            for mfa in user.mfa_devices.all():
                result.append(IamUserMfaDeviceModel(
                    user_id=user.user_id,
                    serial_number=mfa.serial_number,
                    enable_date=mfa.enable_date
                ))

        return result
