import boto3

from .base import BaseResource
from .model.models import SubnetModel


class Subnets(BaseResource):
    __model__ = SubnetModel
    __resource_name__ = "Subnets"

    @staticmethod
    def list():
        from .util import AwsTagsUtil

        client = boto3.client('ec2')
        items = client.describe_subnets()['Subnets']
        result = []
        for item in items:
            record = SubnetModel(id=item['SubnetId'],
                                 name=AwsTagsUtil.extract_name_from_tags(item['Tags']),
                                 vpc_id=item['VpcId'],
                                 cidr_block=item['CidrBlock'],
                                 availability_zone=item['AvailabilityZone'],
                                 state=item['State'],
                                 map_public_ip_on_launch=item['MapPublicIpOnLaunch'],
                                 default_for_az=item['DefaultForAz'],
                                 available_ip_address_count=item['AvailableIpAddressCount'],
                                 assign_ipv6_address_on_creation=item['AssignIpv6AddressOnCreation'],
                                 cfn_details=AwsTagsUtil.extract_cloudformation_from_tags(item['Tags']))
            result.append(record)

        return result
