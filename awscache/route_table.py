import boto3

from .base import BaseResource
from .model.models import RouteTableAssociationModel, RouteTableEntryModel, RouteTableModel


class RouteTables(BaseResource):
    __model__ = RouteTableModel
    __resource_name__ = "Route Tables"

    @staticmethod
    def list():
        from .util import AwsTagsUtil

        client = boto3.client('ec2')
        items = client.describe_route_tables()['RouteTables']
        result = []
        for item in items:
            result.append(RouteTableModel(
                id=item['RouteTableId'],
                name=AwsTagsUtil.extract_name_from_tags(item['Tags']),
                vpc_id=item['VpcId'],
                tags=item['Tags']))

            for association in item['Associations']:
                result.append(RouteTableAssociationModel(id=association['RouteTableAssociationId'],
                                                         route_table_id=association['RouteTableId'],
                                                         subnet_id=association.get('SubnetId', None),
                                                         main=association['Main']))

            i = 0
            for route in item['Routes']:
                result.append(RouteTableEntryModel(
                    index=i,
                    route_table_id=item['RouteTableId'],
                    state=route.get('State', None),
                    origin=route.get('Origin', None),
                    destination_cidr_block=route.get('DestinationCidrBlock', None),
                    destination_prefix_list_id=route.get('DestinationPrefixListId', None),
                    gateway_id=route.get('GatewayId', None),
                    nat_gateway_id=route.get('NatGatewayId', None),
                    vpc_peering_connection_id=route.get('VpcPeeringConnectionId', None),
                    instance_id=route.get('InstanceId', None),
                    instance_owner_id=route.get('InstanceOwnerId', None),
                    network_interface_id=route.get('NetworkInterfaceId', None),
                    destination_ipv6_cidr_block=route.get('DestinationIpv6CidrBlock', None),
                    egress_only_igw_id=route.get('EgressOnlyInternetGatewayId', None)
                ))
                i += 1

        return result
