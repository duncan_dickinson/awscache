import boto3

from .base import BaseResource
from .model.models import ElasticIpModel


class ElasticIps(BaseResource):
    __model__ = ElasticIpModel
    __resource_name__ = "Elastic IPs"

    @staticmethod
    def list():
        client = boto3.client('ec2')
        result = []
        for item in client.describe_addresses()['Addresses']:
            result.append(
                ElasticIpModel(
                    id=item['NetworkInterfaceId'],
                    network_interface_owner_id=item['NetworkInterfaceOwnerId'],
                    domain=item['Domain'],
                    allocation_id=item['AllocationId'],
                    public_ip=item['PublicIp'],
                    private_ip=item['PrivateIpAddress'],
                    network_interface_id=item['NetworkInterfaceId'],
                    instance_id=item.get('InstanceId', None),
                    association_id=item['AssociationId']))
        return result
