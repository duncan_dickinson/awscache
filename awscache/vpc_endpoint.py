import boto3

from .base import BaseResource
from .model.models import VpcEndpointModel


class VpcEndpoints(BaseResource):
    __model__ = VpcEndpointModel
    __resource_name__ = "VPC Endpoints"

    @staticmethod
    def list():
        client = boto3.client('ec2')
        item_listing = client.describe_vpc_endpoints()
        result = []

        while True:
            for item in item_listing['VpcEndpoints']:
                result.append(VpcEndpointModel(
                    id=item['VpcEndpointId'],
                    vpc_id=item['VpcId'],
                    state=item['State'],
                    creation_timestamp=item['CreationTimestamp'],
                    service_name=item['ServiceName'],
                    policy=item['PolicyDocument']))

            if 'NextToken' in item_listing:
                item_listing = client.describe_vpc_endpoints(NextToken=item_listing['NextToken'])
            else:
                break

        return result
