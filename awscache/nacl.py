import boto3

from .base import BaseResource
from .model.models import NaclAssociationModel, NaclEntryModel, NaclModel


class Nacls(BaseResource):
    __model__ = NaclModel
    __resource_name__ = "Network Access Controls"

    @staticmethod
    def list():
        client = boto3.client('ec2')
        items = client.describe_network_acls()['NetworkAcls']
        result = []
        for item in items:
            result.append(NaclModel(
                id=item['NetworkAclId'],
                vpc_id=item['VpcId'],
                default=item['IsDefault'],
                tags=item['Tags']))

            for association in item['Associations']:
                result.append(NaclAssociationModel(
                    id=association['NetworkAclAssociationId'],
                    nacl_id=association['NetworkAclId'],
                    subnet_id=association['SubnetId']))

            index = 0
            for entry in item['Entries']:
                if 'IcmpTypeCode' in entry:
                    icmp_type = entry['IcmpTypeCode']['Type']
                    icmp_code = entry['IcmpTypeCode']['Code']
                else:
                    icmp_type = None
                    icmp_code = None

                if 'PortRange' in entry:
                    port_range_from = entry['PortRange']['From']
                    port_range_to = entry['PortRange']['To']
                else:
                    port_range_from = None
                    port_range_to = None

                result.append(NaclEntryModel(
                    nacl_id=item['NetworkAclId'],
                    index=index,
                    rule_number=entry['RuleNumber'],
                    protocol=entry['Protocol'],
                    rule_action=entry['RuleAction'],
                    egress=entry['Egress'],
                    cidr_block=entry['CidrBlock'],
                    ipv6_cidr_block=entry.get('Ipv6CidrBlock', None),
                    icmp_type=icmp_type,
                    icmp_code=icmp_code,
                    port_range_to=port_range_to,
                    port_range_from=port_range_from))

                index += 1

        return result
