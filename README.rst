=========
AWS Cache
=========

A demonstration tool that caches results from (some) AWS API calls into an SQLite database.

This code was created to aid in preparing some reporting tools. By caching the data it's possible
to build 1 or more reports without re-querying. As the data can be made available via SQLAlchemy, it's
also possible to utilise relationships between various AWS resources.


Building
________

To use this codebase you'll need:

* Python 3

To build the library::

    python3 setup.py bdist_wheel
    pip install dist/awscache-0.1.0-py3-none-any.whl


Demonstration
_____________
To run the app::

    ./main.py

You'll need to configure your AWS credentials as per http://boto3.readthedocs.io/en/latest/guide/configuration.html
